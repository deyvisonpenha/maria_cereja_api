# maria_cereja_api
## RF
### admin
[X] cadastrar um produto
[X] listar todos os produtos
[X] listar todos os Usuarios
[X] Listar todos os Pedidos
[X] editar produto
[X] Atualizar o status do pedido
[] emitir relatorio de faturamento


### Usuario
[X] logar na aplicação com numero de celular
[X] colocar produto no carrinho
[X] remover um produto do carrinho
[X] Deletar carrinho
[X] fazer um pedido (finalizar compra)
[X] Acompanhar o status do pedido
[X] historico de pedidos

## RNF
[X] produto -> id + nome , foto , valor , categoria
[X] cart -> id + [ {produto_id + quantidade + valorTotal}]+ user_id + finalizar: boolean  
[X] checkout -> id + user_id + cart_id + nome e end + total + status: string(em análise | aceito | recusado)

## RN
[] upload e armazenamento das fotos

repository - é o detentor das operações (buscar, deletar, criar... etc) que vamos fazer em cima dos dados. os dados não podem estar acecíveis externamente.