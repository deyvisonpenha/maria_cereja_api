import mongoose from "mongoose";
require('dotenv').config();

const uri = process.env.MONGODB_HOST;

mongoose.connect(uri, 
  { 
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  });

const connection = mongoose.connection;

connection.once("open", function() {
  console.log("MongoDB database connection established successfully");
});
