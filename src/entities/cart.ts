import mongoose, { Schema, Document } from 'mongoose';
import { UserInterface } from './user';
import { ProductInterface } from './product';

interface ProductToCart {
  productId: ProductInterface['_id'];
  productQuantity?: number;
  total?: number;
}

export interface CartInterface extends Document {
  products: [ProductToCart];
  userId: UserInterface['_id'];
  finished?: boolean;
  totalToBuy: number;
}

const cartSchema: Schema = new Schema({
  products: {
      type: [Schema.Types.Mixed],
      //ref: 'Products',
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'Users'
    },
    finished: {
      type: Boolean,
      default: false
    },
    totalToBuy: Number,
  },
  {
    timestamps: true,
  });

  export default mongoose.model<CartInterface>('Carts', cartSchema );