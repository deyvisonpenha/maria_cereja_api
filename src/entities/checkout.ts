import mongoose, { Schema, Document } from 'mongoose';
import { UserInterface } from './user';
import { CartInterface } from './cart';

export interface CheckoutInterface extends Document {
    userId: UserInterface['_id'];
    cartId: CartInterface['_id'];
    name: string;
    address: string;
    total: number;
    status?: 'Aceito' | 'Recusado' | 'Aguardando Confirmação';
}

const checkoutSchema: Schema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'Users'
    },
    cartId: {
        type: Schema.Types.ObjectId,
        ref: 'Carts'
    },
    name: String,
    address: String,
    total: Number,
    status:{
        type: String,
        default: "Aguardando Confirmação",//'Aceito' | 'Recusado' | 'Aguardando Confirmação'
        } 
    },
  {
    timestamps: true,
  });

  export default mongoose.model<CheckoutInterface>('Orders', checkoutSchema );