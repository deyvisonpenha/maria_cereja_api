import mongoose, { Schema, Document } from 'mongoose';

export interface ProductInterface extends Document {
    name: string;
    photos: string[];
    value: number;
    category: string;
    size: string;
    colors: string[];
    amountStock: number;
} 

const productSchema: Schema = new Schema({
    name: String,
    photos: [String],
    value: Number,
    category: String,
    size: String,
    colors: [String],
    amountStock: Number,
    },
  {
    timestamps: true,
  });

  export default mongoose.model<ProductInterface>('Products', productSchema );