import mongoose, { Schema, Document } from 'mongoose';

export interface UserInterface extends Document {
	phone: string;
  }

const userSchema: Schema = new Schema({
	phone: {
			type: String,
			unique: true,
			required: true
			}
	},
    {
    timestamps: true,
    });

  export default mongoose.model<UserInterface>('Users', userSchema );