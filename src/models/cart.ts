import {Entity, ObjectIdColumn, ObjectID, Column, CreateDateColumn, UpdateDateColumn} from 'typeorm';
import User from './user';

@Entity('carts')
class Cart {
@ObjectIdColumn()
id: ObjectID;

@Column()
products: Array<object>;

@Column()
user: User;

@Column({ default: false })
finished: boolean;

@Column()
totalToBuy: number;

@CreateDateColumn()
created_at: string;

@UpdateDateColumn()
updated_at: string;

}

export default Cart;