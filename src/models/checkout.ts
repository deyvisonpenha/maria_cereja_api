import {Entity, ObjectIdColumn, ObjectID, Column, CreateDateColumn, UpdateDateColumn} from 'typeorm';

@Entity('orders')
class Checkout {
@ObjectIdColumn()
id: ObjectID;

@Column()
userId: string;

@Column()
cartId: string;

@Column()
name: string;

@Column()
address: string;

@Column('decimal')
total: number;

@Column()
status: 'aceito' | 'recusado' | 'análise';

@CreateDateColumn()
created_at: string;

@UpdateDateColumn()
updated_at: string;

}

export default Checkout;