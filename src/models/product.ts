import {Entity, ObjectIdColumn, ObjectID, Column, CreateDateColumn, UpdateDateColumn} from 'typeorm';

@Entity('products')
class Product {
@ObjectIdColumn()
id: ObjectID;

@Column()
name: string;

@Column()
photos: string[];

@Column('decimal')
value: number;

@Column()
category: string;

@Column()
size: string;

@Column({ nullable: true})
colors: string[];

@Column({ nullable: true })
amountStock: number;

@CreateDateColumn()
created_at: string;

@UpdateDateColumn()
updated_at: string;

}

export default Product;