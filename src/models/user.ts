import {Entity, ObjectIdColumn, ObjectID, Column, CreateDateColumn, UpdateDateColumn} from 'typeorm';

@Entity('users')
class User {
@ObjectIdColumn()
id: ObjectID;

@Column({ unique: true })
phone: string;

@CreateDateColumn()
created_at: string;

@UpdateDateColumn()
updated_at: string;
}

export default User;