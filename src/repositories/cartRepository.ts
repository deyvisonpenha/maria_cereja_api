import Cart, { CartInterface } from '../entities/cart';
import {ProductInterface} from '../entities/product'
var ObjectID = require('mongodb').ObjectID;

interface Response {
  message: string,
  code: number
}

class CartRepository {

  public async all(): Promise<CartInterface[]> {
    const allCart = await Cart.find();
    
    return allCart;
  }

  public async deleteCart(cartId: CartInterface["_id"]): Promise<Response>{
    try{
      await Cart.findOneAndDelete({_id: ObjectID(cartId)});
      return {message: "Carrinho Deletado com Sucesso", code: 200}
    }catch {
    return {message: "error ao deletar carrinho", code: 400}
  }
  }

  public async removeProductInCart(
    productId: ProductInterface["_id"], 
    cartId: CartInterface["_id"]
  ): Promise<Response>{
    try{
      await Cart.updateOne(
        {
        "products.productId": ObjectID(productId),
        _id: ObjectID(cartId)
        },
        {$pull: { products: {productId: ObjectID(productId)}}},
        {upsert : true }
      );
      return {message: "Item Deletado com Sucesso", code: 200}
  } catch(err) {
      return {message: "error ao deletar item", code: 400}
  }
  }

}

export default CartRepository;