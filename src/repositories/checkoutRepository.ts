import Checkout, { CheckoutInterface } from '../entities/checkout';
import Cart from '../entities/cart';

interface ICreateCheckout {
    userId: CheckoutInterface['userId'], 
    cartId: CheckoutInterface['cartId'], 
    name: CheckoutInterface['name'], 
    address: CheckoutInterface['address'], 
}

interface Response {
    order: ICreateCheckout;
    message: string;
    code: number;
}

var ObjectID = require('mongodb').ObjectID;

class CheckoutRepository {

    public async allByUser(userId: string): Promise<CheckoutInterface[]> {
        const checkout = await Checkout.find({where: {userId}, order: { created_at: -1}});
        
        return checkout;
    }

    public async all(): Promise<CheckoutInterface[]>{
        const checkout = await Checkout.find();
        
        return checkout;
    }

    public async showStatusCheckout(userId: string){
        try{
            const checkoutStatus = await Checkout.findOne(
                {_id: ObjectID(userId)},
                //{order: { created_at: -1}}
            );
              
            return checkoutStatus;

        }catch(err){
            return err;
        }
    }

    public async create({ userId, cartId, name, address }: ICreateCheckout): Promise<Response> {        
        let total = 0;
        
        try{
            const {totalToBuy} = await Cart.findOneAndUpdate(
                {_id: ObjectID(cartId)},
                {$set: {"finished": true}},
                {returnOriginal: false}
            ); 

            total = totalToBuy;
        }catch(err){
            return {order: null, message: "error ao atualizar carrinho", code: 400}
        }
        
        try{
            const checkout = await Checkout.create({ 
                userId, 
                cartId, 
                name, 
                address , 
                total
            });
            return {order: checkout, message: "compra finalizada com sucesso", code: 200};
        }catch {
            return {order: null, message: "error ao finalizar compra", code: 400}
        }
    }

    public async updateStatus(checkoutId: string, newStatus: CheckoutInterface['status']): Promise<object>{
      const { value } = await Checkout.findOneAndUpdate(
        {_id:  ObjectID(checkoutId)},
        {$set: {status: newStatus}},
        {returnOriginal: false}
      );

      if(!value)
      throw new Error("erro durante a atualização");

      return {
        message: `Status atualizado para ${newStatus}`,
        status: newStatus
      };
    }
}

export default CheckoutRepository;
