import Product, { ProductInterface } from '../entities/product';

interface Request {
  name?: ProductInterface['name'];
  photos?: ProductInterface['photos'];
  value?: ProductInterface['value'];
  category?: ProductInterface['category'];
  size?: ProductInterface['size'],
  colors?: ProductInterface['colors'],
  amountStock?: ProductInterface['amountStock'];
}

class productRepository {

  public async all(): Promise<ProductInterface[]> {
    const allProducts = await Product.find();
    
    return allProducts;
  }
  
  public async create(
    {
      name,
      photos,
      value,
      category,
      size,
      colors,
      amountStock,
    }: Request
  ): Promise<ProductInterface> {

  try {
    const newProduct = await Product.create({
      name,
      photos,
      value,
      category,
      size,
      colors,
      amountStock,
    });

    return newProduct;

    } catch{
      throw new Error('cant create products')
    }
  }

  public async update(
    productAtributesUpdate: Request, 
    product_id: string
  ): Promise<ProductInterface> {

      var ObjectID = require('mongodb').ObjectID;
      
      const {value: updatedProduct} = await Product.findOneAndUpdate(
        {_id:  ObjectID(product_id)},
        {$set: productAtributesUpdate},
        {returnOriginal: false}
      );
      
      return updatedProduct;
  }

}

export default productRepository;