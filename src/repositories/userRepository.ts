import CreateSessionService from '../services/CreateSessionService';
import User from '../entities/user';

class userRepository {

  public async all() {
    const users = await User.find();

    return users;
  }

  public async create(phone: string) {

    const createSessionService = new CreateSessionService();

    const user = await createSessionService.execute(phone);

    //console.log(user.phone)

    return user;
  }
}

export default userRepository;