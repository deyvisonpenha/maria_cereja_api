import { Router } from 'express';
import CreateCarService from '../services/CreateCarService';
import Cart from '../entities/cart';
import CartRepository from '../repositories/cartRepository';
import {ProductInterface} from '../entities/product'

const cartRoutes = Router();
const cartRepository = new CartRepository();
var ObjectID = require('mongodb').ObjectID;

cartRoutes.get('/', async (request, response) => {
    const carts = await cartRepository.all();

    return response.json(carts);
})

cartRoutes.post('/:userId', async (request, response) => {
    const { product_id, productQuantity } = request.body;
    const { userId } = request.params;

    const createCarService = new CreateCarService();

    const cart = await createCarService.execute({ product_id, userId, productQuantity });

    return response.json(cart);
});

cartRoutes.delete('/cancel/:cartId', async (request, response) => {
    const {cartId} = request.params;

    const cartRequest = await cartRepository.deleteCart(cartId);

    return response.status(cartRequest.code).json({message: cartRequest.message});
})

cartRoutes.delete('/:cartId/product/:productId', async (request, response) => {
    const {productId, cartId} =request.params;

    const cartRequest = await cartRepository.removeProductInCart(productId, cartId);

    return response.status(cartRequest.code).json({message: cartRequest.message});
})


export default cartRoutes;