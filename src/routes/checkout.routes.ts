import { Router } from 'express';
import CheckoutRepository from '../repositories/checkoutRepository';

const checkoutRouter = Router();
const checkoutRepository = new CheckoutRepository();


checkoutRouter.get('/admin', async (request, response) => {
    const checkout = await checkoutRepository.all();

    return response.json(checkout);
});

checkoutRouter.get('/statuslastcheckout/:userId', async (request, response) => {
    const { userId } = request.params;
    
    const statusLastCheckout = await checkoutRepository.showStatusCheckout(userId);

    return response.json(statusLastCheckout); 
});

checkoutRouter.get('/user/:userId', async (request, response) => {
    const {userId} = request.params;

    const checkout = await checkoutRepository.allByUser(userId);

    return response.json(checkout);
});

checkoutRouter.post('/', async (request, response) => {
    const { userId, cartId, name, address } = request.body;

    const checkout = await checkoutRepository.create({ userId, cartId, name, address });

    return response.status(checkout.code).json({
            message: checkout.message,
            order: checkout.order
        });
});

checkoutRouter.patch('/updateStatus/:checkoutId', async (request, response) => {
    const { checkoutId } = request.params;
    const { newStatus } = request.body;

    const statusUpdated = await checkoutRepository.updateStatus(checkoutId, newStatus);

    return response.status(200).json(statusUpdated);
});

export default checkoutRouter;