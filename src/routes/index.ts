import { Router } from 'express';

import UserRoutes from './user.routes';
import SessionRoutes from './session.route';
import ProductRouter from './product.routes';
import CartRouter from './cart.routes';
import mailRouter from './mail.routes';
import checkoutRouter from './checkout.routes';

const routes = Router();

routes.use('/users', UserRoutes);
routes.use('/sessions', SessionRoutes);
routes.use('/products', ProductRouter);
routes.use('/cart', CartRouter);
routes.use('/mail', mailRouter);
routes.use('/checkout', checkoutRouter);

export default routes;