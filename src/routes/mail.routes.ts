import { Router} from 'express';
import MailJet from 'node-mailjet';

const mailRouter = Router();

const mailJetConnection = MailJet.connect('f38cd98290edc7ba59f38220f34eb0a1', '122560dac581e2a7f4d5a8c867c9576e');

mailRouter.get('/', (request, response) => {
    const mailJetRequest = mailJetConnection.post("send", {'version': 'v3.1'})
    .request({
            "Messages":[
                {
                "From": {
                    "Email": "pedeae.business@gmail.com",
                    "Name": "Brayhon"
                },
                "To": [
                    {
                    "Email": "deyvisonpenha1@gmail.com",
                    "Name": "Deyvison"
                    }
                ],
                "Subject": "Greetings from Mailjet.",
                "TextPart": "My first Mailjet email",
                "HTMLPart": "<h3>Dear passenger 1, welcome to <a href='https://www.mailjet.com/'>Mailjet</a>!</h3><br />May the delivery force be with you!",
                "CustomID": "AppGettingStartedTest"
                }
            ]
        })

    mailJetRequest
    .then((result) => {
        console.log(result.body)
        return result.body;
    })
    .catch((err) => {
        console.log(err.statusCode)
        return err.statusCode;
    })

});

export default mailRouter;
