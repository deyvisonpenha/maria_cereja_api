import { Router } from 'express';
import ProductRepository from  '../repositories/productRepository';

const productRoutes = Router();
const productRepository = new ProductRepository();

productRoutes.get('/', async (request, response)=>{
  const products = await productRepository.all();

  return response.json(products);
})

productRoutes.post('/', async (request, response) => {
    const {
      name,
      photos,
      value,
      category,
      size,
      colors,
      amountStock
    } = request.body;

    const product = await productRepository.create({
      name,
      photos,
      value,
      category,
      size,
      colors,
      amountStock
    });

    return response.json(product);
});

productRoutes.patch('/:product_id', async (request, response) => {
    const productAtributesUpdate = request.body;

    const {product_id} = request.params;

    try{
      const updatedProduct = await productRepository.update( productAtributesUpdate, product_id );

      return response.json(updatedProduct);
    } catch (err) {
      return response.status(400).json({error: err.message})
    }
})

export default productRoutes;