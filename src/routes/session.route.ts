import { Router } from 'express';
import UserRepository from '../repositories/userRepository';

const userRoutes = Router();

userRoutes.post('/', async (request, response) => {
    try{
    const { phone } = request.body;
    
    const createSession = new UserRepository();
    const user = await createSession.create(phone);

    return response.json(user);
    }catch(err){
        return response.status(400).json({error: err.message})
    }
})

export default userRoutes;