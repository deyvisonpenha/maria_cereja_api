import { Router } from 'express';
import UserRepository from '../repositories/userRepository';

const userRoutes = Router();

userRoutes.get('/', async (request, response) => {
  const userRepository = new UserRepository();
  
  const getAllUsers = await userRepository.all();

  return response.json(getAllUsers);
});

export default userRoutes;