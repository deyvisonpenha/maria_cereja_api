import Cart, { CartInterface } from '../entities/cart';
import User from '../entities/user';
import Product, { ProductInterface } from '../entities/product';
import e from 'express';

interface IAddProductToCarRequest {
    product_id: ProductInterface['_id'], 
    userId: CartInterface['userId'], 
    productQuantity: CartInterface['totalToBuy']
}

class AddToCarService {
  public async execute({ product_id, userId, productQuantity }: IAddProductToCarRequest){
    
    var ObjectID = require('mongodb').ObjectID;

    const product = await Product.findOne({ _id: ObjectID(product_id) });

    const total = productQuantity * product.value;

    const existingCart = await Cart.findOne({  
      $and: [
        {finished: false},
        {userId}
      ]
    });
    
    if(!existingCart){
      const products= [ { productId: product._id, productQuantity, total}];
      const cart = await Cart.create({ userId , products, totalToBuy: total});
      return cart;
    }

    const verifyNewProductInCart = existingCart.products.some( (productInCart) => {
      if(productInCart.productId == product_id) {
        return true;
      }
    });

    try{
      if(!verifyNewProductInCart){
        await Cart.updateOne(
          {_id: existingCart._id},
          {
            $push: {
              products: {productId: product._id, productQuantity, total}
            },
            $inc: { totalToBuy: total}
          }
        );
      }
      else{      
        await Cart.updateOne(
          {
            "products.productId": ObjectID(product_id),
            _id: existingCart._id
          },
          {
            $inc: {
              "products.$.productQuantity": productQuantity,
              "products.$.total": total
            }
          }
        );
      }
      
      return existingCart;

    }catch (err){
      throw new Error('cant insert item in your cart')
    }
  }
}

export default AddToCarService;