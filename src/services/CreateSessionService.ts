import User from '../entities/user';

class CreateSessionService {
  public async execute(phone: string) {

    let userFound = await User.findOne({phone});
    
    if(!userFound){
      userFound = new User({ phone });
      
      userFound.save( (error: any) => {
        if(error){
          console.log("error", error)
          return error 
        }
      });
    }

    return userFound
  }
}

export default CreateSessionService;
